/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package dk.xakeps.idgetter;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.data.DataContainer;
import org.spongepowered.api.data.DataQuery;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.state.GamePreInitializationEvent;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.util.Optional;

@Plugin(id = "idgetter",
        name = "IdGetter",
        version = "1.1",
        description = "Get's item id or BlockState id",
        url = "https://spongeapi.com",
        authors = "Xakep_SDK")
public class IdGetter {
    @Listener
    public void onGamePreInit(GamePreInitializationEvent event) {
        CommandSpec getIdCommand = CommandSpec.builder()
                .permission("idgetter.command.getid.base")
                .arguments(GenericArguments.playerOrSource(Text.of("target")))
                .executor((src, args) -> {
                    Player target = args.<Player>getOne(Text.of("target")).get();
                    Optional<ItemStack> itemInHand = target.getItemInHand(HandTypes.MAIN_HAND);
                    if (!itemInHand.isPresent()) {
                        throw new CommandException(Text.of(TextColors.RED, "Main hand is empty!"));
                    }

                    ItemStack stack = itemInHand.get();
                    src.sendMessage(Text.of(TextColors.GREEN, "ItemType ID: ", TextColors.GOLD, stack.getType().getId()));
                    Optional<BlockState> blockState = stack.get(Keys.ITEM_BLOCKSTATE);
                    if (blockState.isPresent()) {
                        src.sendMessage(Text.of(TextColors.GREEN, "BlockState ID: ", TextColors.GOLD, blockState.get().getId()));
                    } else {
                        src.sendMessage(Text.of(TextColors.GREEN, "No BlockState in ItemStack found."));
                    }

                    DataContainer dataContainer = stack.toContainer();
                    Optional<Object> unsafeDamage = dataContainer.get(DataQuery.of("UnsafeDamage"));
                    if (unsafeDamage.isPresent()) {
                        src.sendMessage(Text.of(TextColors.GREEN, "UnsafeDamage: ", TextColors.GOLD, unsafeDamage));
                    } else {
                        src.sendMessage(Text.of(TextColors.GREEN, "No UnsafeDamage in ItemStack found."));
                    }

                    Optional<EntityType> entityType = stack.get(Keys.SPAWNABLE_ENTITY_TYPE);
                    if (entityType.isPresent()) {
                        src.sendMessage(Text.of(TextColors.GREEN, "EntityType ID: ", TextColors.GOLD, entityType.get().getId()));
                    } else {
                        src.sendMessage(Text.of(TextColors.GREEN, "No EntityType in ItemStack found."));
                    }

                    return CommandResult.success();
                })
                .build();
        Sponge.getCommandManager().register(this, getIdCommand, "getid", "gid");
    }
}
